from flask import Flask, request, make_response
import requests
from faker import Faker

from lesson_5.db import execute_query

app = Flask(__name__)


@app.route("/get_btc_rate")
def get_btc_rate():
    try:
        currency = str(request.args.get('currency', 'UAH'))
        get_rate_of_btc = requests.get("https://bitpay.com/api/rates").json()
        for item in get_rate_of_btc:
            if item["code"] == currency:
                print(item)
                return str(item['rate'])
    except ValueError:
        print("You value isn't appropriate for the task")


@app.route('/random_users')
def gen_random_users():
    fake = Faker()
    count = int(request.args.get('count', 100))
    names = []
    for item in range(count):
        names.append(fake.name())
        names.append(fake.email())
    response = make_response("\n".join(names))
    response.headers["Content-Type"] = "text/plain"
    return response


@app.route("/unique_names")
def get_unique_names():
    got_results = execute_query("""SELECT FirstName 
    FROM customers 
    GROUP BY FirstName;
    """)
    unique_names = []
    for item in got_results:
        unique_names.append(str(item))
    response = make_response("\n".join(unique_names))
    response.headers["Content-Type"] = "text/plain"
    return response


@app.route("/tracks_count")
def get_tracks_count():
    counted = execute_query("""SELECT COUNT(*) 
                                   FROM tracks;""")
    return str(counted)


@app.route("/customers")
def get_customers():
    city = str(request.args.get('city', "Oslo"))
    country = str(request.args.get('country', "Norway"))
    matched_customer = execute_query("""SELECT * FROM customers
                                        WHERE City = ? AND Country = ?;""", (city, country))
    response = make_response("".join(str(matched_customer)))
    response.headers["Content-Type"] = "text/plain"
    return response


app.run(debug=True)
